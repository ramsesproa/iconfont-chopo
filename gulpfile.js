/*var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');


 
gulp.task('iconfont', function(){
  return gulp.src(['assets/*.svg'])
    .pipe(iconfont({
      fontName: 'myfont', // required
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
    }))
      .on('glyphs', function(glyphs, options) {
        // CSS templating, e.g.
        console.log(glyphs, options);
      })
    .pipe(gulp.dest('assets/fonts/'));
});

gulp.task('default', gulp.series('iconfont'));*/

/*var gulp         = require('gulp');
var iconfont     = require('gulp-iconfont');
var iconfontCss  = require('gulp-iconfont-css');
var iconfontHTML = require('gulp-iconfont-template');
var svgicons2svgfont = require('gulp-svgicons2svgfont');

var fontName = 'iconfont-chopo';
var runTimestamp = Math.round(Date.now()/1000);
 
gulp.task('iconfont', function(){
  gulp.src(['assets/svg/*.svg'])
    .pipe(iconfontHTML({
      fontName: fontName,
      fontPath: '../',
      targetPath: '../index.html',
    }))
    .pipe(iconfontCss({
      fontName: fontName,
    //  path: 'assets/css/templates/_icons.scss',
      targetPath: '../'+fontName+'.css',
      fontPath: 'fonts/'
    }))
    .pipe(iconfont({
      fontName: fontName,
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff','woff2','svg'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
     }))
    .on('glyphs', function(glyphs, options) {
        // CSS templating, e.g.
        console.log(glyphs, options);
      })
    .pipe(gulp.dest('dist/fonts/'));
});


 
gulp.task('svg2icon', function(){
    svgicons2svgfont(['assets/svg/*.svg'], {
      fontName: fontName
    })
    .on('glyphs', function(glyphs) {
      console.log(glyphs);
      // Here generate CSS/SCSS  for your glyphs ...
    })
    .pipe(gulp.dest('www/font/'));
});*/


var gulp = require('gulp'),
    iconfontCss  = require('gulp-iconfont-css'),
    iconfontHTML = require('gulp-iconfont-template'),
    consolidate = require('gulp-consolidate'),
    iconfont = require('gulp-iconfont');

var runTimestamp = Math.round(Date.now()/1000);
var fontName = 'iconfont-chopo';

/*gulp.task('iconfont', function(){
  return gulp.src(['assets/svg/*.svg'])
    .pipe(iconfontHTML({
      fontName: fontName,
      //path: 'assets/index.html',
      fontPath: '../',
      targetPath: '../index.html',
    }))
    .pipe(iconfontCss({
      fontName: fontName,
    //  path: 'assets/css/templates/_icons.scss',
      targetPath: '../'+fontName+'.css',
      fontPath: 'fonts/'
    }))
    .pipe(iconfont({
      fontName: fontName, // required
      prependUnicode: true, // recommended option
      fontHeight: 1001,
      normalize: true,
      formats: ['ttf', 'eot', 'woff','woff2','svg'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
    }))
    .on('glyphs', function(glyphs, options) {
       console.log(glyphs, options);
    })
    .pipe(gulp.dest('dist/fonts/'));
});*/

gulp.task('iconfont', function () {
   return gulp.src('assets/svg/*.svg')
        .pipe(iconfont({
//            fontName: fontName,
//            formats: ['ttf', 'eot', 'woff', 'woff2'],
//            appendCodepoints: true,
//            prependUnicode: true,
//            //appendUnicode: false,
//            normalize: true,
//            fontHeight: 1000,
//            centerHorizontally: true,
//            timestamp: runTimestamp
      fontName: fontName,
      fontHeight: 1001,
      normalize: true,
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff','woff2','svg'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
        }))
        .on('glyphs', function (glyphs, options) {
            console.log(glyphs, options);
            gulp.src('assets/iconfont.css')
                .pipe(consolidate('underscore', {
                    glyphs: glyphs,
                    fontName: fontName,
                    fontDate: new Date().getTime()
                }))
                .pipe(gulp.dest('dist'));

            gulp.src('assets/index.html')
                .pipe(consolidate('underscore', {
                    glyphs: glyphs,
                    fontName: fontName
                }))
                .pipe(gulp.dest('dist'));
        })
        .pipe(gulp.dest('dist/fonts'));
});

//gulp.task('default', gulp.series('iconfont'));